import RPi.GPIO as GPIO
import dht11
import time
import datetime
import os

# initialize GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.cleanup()

# read data using pin 14
instance = dht11.DHT11(pin=2)
result = instance.read()
if result.is_valid():
    print("Last valid input: " + str(datetime.datetime.now()))
    print("Temperature: %d C" % result.temperature)
    print("Humidity: %d %%" % result.humidity)
with open('/home/pi/Desktop/data.txt', 'w') as the_file:
    the_file.write(str(result.temperature))
    the_file.write('\n')
    the_file.write(str(result.humidity))

  