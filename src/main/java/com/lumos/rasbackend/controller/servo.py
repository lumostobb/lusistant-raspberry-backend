import RPi.GPIO as GPIO
import time

servoPIN = 18
GPIO.setmode(GPIO.BCM)
GPIO.setup(servoPIN, GPIO.OUT)

p = GPIO.PWM(servoPIN, 50)
p.start(2.5)

p.ChangeDutyCycle(0)
time.sleep(0.5)
p.ChangeDutyCycle(2.5)
time.sleep(0.5)
p.ChangeDutyCycle(5.0)
time.sleep(0.5)
p.ChangeDutyCycle(7.5)
time.sleep(5)
p.ChangeDutyCycle(5)
time.sleep(0.5)
p.ChangeDutyCycle(2.5)
time.sleep(0.5)

p.stop()
GPIO.cleanup()