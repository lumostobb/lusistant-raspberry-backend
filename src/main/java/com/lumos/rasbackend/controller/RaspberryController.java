package com.lumos.rasbackend.controller;

import com.lumos.rasbackend.model.Command;
import com.lumos.rasbackend.model.Pin;
import com.lumos.rasbackend.repository.CommandRepository;
import com.lumos.rasbackend.repository.PinRepository;
import com.pi4j.io.gpio.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.pi4j.component.motor.impl.GpioStepperMotorComponent;
import com.pi4j.component.servo.ServoDriver;
import com.pi4j.component.servo.ServoProvider;
import com.pi4j.component.servo.impl.RPIServoBlasterProvider;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

@RestController
public class RaspberryController{

    GpioController gpio = GpioFactory.getInstance();
    public GpioPinDigitalOutput isikKirmizi  = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_00,"My;led2",PinState.LOW);
    public GpioPinDigitalOutput isikYesil  = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_02,"My;led3",PinState.LOW);
    public GpioPinDigitalOutput isikSari  = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_03,"My;led4",PinState.LOW);
    public GpioPinDigitalOutput ses= gpio.provisionDigitalOutputPin(RaspiPin.GPIO_26,"My;led5",PinState.LOW);
    public Process guvenlik = null;
    public Process distance = null;
    public Runtime rt= Runtime.getRuntime();

    static {
        System.setProperty("pi4j.linking", "dynamic");
    }
    @Autowired
    private CommandRepository commandRepository;

    @Autowired
    private PinRepository pinRepository;


    @PostMapping("/komutEkle")
    private ResponseEntity addCommand(@RequestBody Command command){
        Command result = commandRepository.save(command);
        List<Pin> pinList = result.getPins();
        for (Pin pin : pinList) {
            pinRepository.save(pin);
        }
        if (result != null) {
            return new ResponseEntity(result, HttpStatus.OK);
        }
        else
            return new ResponseEntity("Command cannot be created", HttpStatus.BAD_REQUEST);
    }



    @GetMapping("/raspi/{commandName}")
    private ResponseEntity executeCommand(@PathVariable String commandName) throws InterruptedException , IOException{


        if (commandName.equals("isikAc")) {
            
            try {

                isikKirmizi.high();

            } catch (Exception e) {
                e.printStackTrace();
            }


            return  new ResponseEntity("Işık açıldı.",HttpStatus.OK);
        }
        else if(commandName.equals("isikKapat")) {

            try {

                isikKirmizi.low();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return  new ResponseEntity("Işık kapatıldı..",HttpStatus.OK);

        }
        else if(commandName.equals("ocakAc")) {//yesil ac
            try {

                isikYesil.high();

            } catch (Exception e) {
                e.printStackTrace();
            }


            return  new ResponseEntity("Ocak açıldı.",HttpStatus.OK);

        }
        else if (commandName.equals("ocakKapat")) {//yesil kapat

            try {

                isikYesil.low();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return  new ResponseEntity("Ocağım söndü..",HttpStatus.OK);
        }
        else if (commandName.equals("kapiAc")) {//sari ac
            try {

                isikSari.high();

            } catch (Exception e) {
                e.printStackTrace();
            }


            return  new ResponseEntity("Kapı acıldı.",HttpStatus.OK);

        }
        else if(commandName.equals("kapiKapat")) {//sari kapat

            try {

                isikSari.low();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return  new ResponseEntity("Kapı kapatıldı.",HttpStatus.OK);
        }
        else if (commandName.equals("cicekSula")) {
            String komut = "python /home/pi/Desktop/lusistant-raspberry-backend/src/main/java/com/lumos/rasbackend/controller/servo.py ";
            Process p = null;
            try {
                 p=rt.exec(komut);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return new ResponseEntity("Çiçek sulandı.",HttpStatus.OK);
        }
        else if(commandName.equals("sesAc")) {
                        
            try {
                ses.high();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return  new ResponseEntity("Ses açıldı.",HttpStatus.OK);
            
        }
        else if(commandName.equals("sesKapat")) {
            
                                    
            try {
                ses.low();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return  new ResponseEntity("Ses kapandı.",HttpStatus.OK);
            
        }
        else if (commandName.equals("isiOlc")) {

            ArrayList<String> datas = new ArrayList<String>();

            String komut = "python /home/pi/Desktop/Adafruit_Python_DHT/examples/AdafruitDHT.py 11 2 ";
            Process p = null;
            try {
                p=rt.exec(komut);
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                File file = new File("/home/pi/Desktop/data.txt");
                FileReader fileReader = new FileReader(file);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                StringBuffer stringBuffer = new StringBuffer();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    datas.add(line);
                    stringBuffer.append(line);
                    stringBuffer.append("\n");
                }
                fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


            Map<String, String> resultMap = new HashMap<>();
            resultMap.put("Sicaklik", datas.get(0));
            resultMap.put("Nem", datas.get(1));

            return new ResponseEntity<>(resultMap, HttpStatus.OK);

        }
        else if (commandName.equals("guvenlikAc")) {
            String komut = "python /home/pi/Desktop/lusistant-raspberry-backend/src/main/java/com/lumos/rasbackend/controller/guvenlik.py";
            if(guvenlik == null)
                guvenlik=rt.exec(komut);

            return  new ResponseEntity("Güvenlik aktif hale getirildi.",HttpStatus.OK);
        }
        else if(commandName.equals("guvenlikKapat")) {
            
            System.out.println("GUVENLIK KAPATILIYOR");
            if(guvenlik != null){
               guvenlik.destroy();
                if(guvenlik.isAlive()) {
                    guvenlik.destroyForcibly();
                } 
                guvenlik = null;
            }
            
        
        return  new ResponseEntity("Güvenlik devre dışı bırakıldı.",HttpStatus.OK);
    }
        else if(commandName.equals("mesafeOlc")) {
            String komut = "python /home/pi/Desktop/lusistant-raspberry-backend/src/main/java/com/lumos/rasbackend/controller/src.py";
            try {
                distance=rt.exec(komut);
                Thread.sleep(3000);
                distance.destroy();
            } catch (IOException e) {
                e.printStackTrace();
            }
            ArrayList<String> datas = new ArrayList<String>();
            try {
                File file = new File("/home/pi/Desktop/distance.txt");
                FileReader fileReader = new FileReader(file);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                StringBuffer stringBuffer = new StringBuffer();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    datas.add(line);
                    stringBuffer.append(line);
                    stringBuffer.append("\n");
                }
                fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Map<String, String> resultMap = new HashMap<>();
            resultMap.put("Mesafe", datas.get(0));
            return new ResponseEntity<>(resultMap, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>("Komut bulunamadı.", HttpStatus.BAD_REQUEST);
        }

    }


    @DeleteMapping("/{commandName}")
    private ResponseEntity deleteCommand(@PathVariable String commandName) {
        boolean result = commandRepository.deleteByCommandName(commandName);

        return new ResponseEntity(result, HttpStatus.OK);
    }

}
