import RPi.GPIO as GPIO
import time
from gpiozero import Buzzer
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
buzzer = Buzzer(12)
TRIG = 21
ECHO = 20

print "HC-SR04 mesafe sensoru"

GPIO.setup(TRIG,GPIO.OUT)
GPIO.setup(ECHO,GPIO.IN)


GPIO.output(TRIG, False)
print "Olculuyor..."
time.sleep(2)

GPIO.output(TRIG, True)
time.sleep(0.00001)
GPIO.output(TRIG, False)

while GPIO.input(ECHO)==0:
    pulse_start = time.time()

while GPIO.input(ECHO)==1:
    pulse_end = time.time()

pulse_duration = pulse_end - pulse_start

distance = pulse_duration * 17150
distance = round(distance, 2)
with open('/home/pi/Desktop/distance.txt', 'w') as the_file:
    the_file.write(str(distance-0.5))
