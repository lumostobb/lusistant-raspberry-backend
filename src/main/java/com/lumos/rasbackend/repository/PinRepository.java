package com.lumos.rasbackend.repository;

import com.lumos.rasbackend.model.Pin;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PinRepository extends MongoRepository<Pin, String>  {
}
