package com.lumos.rasbackend.repository;

import com.lumos.rasbackend.model.Command;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CommandRepository extends MongoRepository<Command, String>  {
    Command findByCommandName(String commandName);

    boolean deleteByCommandName(String commandName);
}

