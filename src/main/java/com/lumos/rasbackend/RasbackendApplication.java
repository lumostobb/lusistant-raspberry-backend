package com.lumos.rasbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RasbackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(RasbackendApplication.class, args);
    }

}
