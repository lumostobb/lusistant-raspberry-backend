package com.lumos.rasbackend.model;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document("commands")
public class Command {

    @Indexed
    private String commandName;
    private List<Pin> pins = new ArrayList<>();
    private String positiveMessage;
    private String negativeMessage;
    private int onOrOff;

    public Command() {

    }

    public Command(String commandName, List<Pin> pins, String positiveMessage, String negativeMessage) {
        super();
        this.commandName = commandName;
        this.pins = pins;
        this.positiveMessage = positiveMessage;
        this.negativeMessage = negativeMessage;
    }

    public String getCommandName() {
        return commandName;
    }

    public List<Pin> getPins() {
        return pins;
    }

    public String getPositiveMessage() {
        return positiveMessage;
    }

    public String getNegativeMessage() {
        return negativeMessage;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public void setPins(List<Pin> pins) {
        this.pins = pins;
    }

    public void setPositiveMessage(String positiveMessage) {
        this.positiveMessage = positiveMessage;
    }

    public void setNegativeMessage(String negativeMessage) {
        this.negativeMessage = negativeMessage;
    }

    public int getOnOrOff() {
        return onOrOff;
    }

    public void setOnOrOff(int onOrOff) {
        this.onOrOff = onOrOff;
    }
}
